var express=require('express'),
app = express(),
routes = require('./routes'),
partials = require('express-partials');
env = require('./env'),
config = require('./config/' + env.name);

app.set('view engine','ejs');

app.use(partials());
app.use(express.static('static'));

app.get('/', routes.index);
app.get('/:uname',routes.blogDetail);

app.listen(config.port);
console.log('app server running on port '+ config.port);