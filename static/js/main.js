$(document).ready(function() {

    /* ======= Scrollspy ======= */
   $('body').scrollspy({ target: '#page-nav-wrapper', offset: 100});
    
    /* ======= ScrollTo ======= */
    $('.scrollto').on('click', function(e){
        
        //store hash
        var target = this.hash;
                
        e.preventDefault();
        
		$('body').scrollTo(target, 800, {offset: -60, 'axis':'y'});
		
	});
	
	/* ======= Fixed page nav when scrolled ======= */    
    $(window).on('scroll resize load', function() {
        
        $('#page-nav-wrapper').removeClass('fixed');
         
         var scrollTop = $(this).scrollTop();
         var topDistance = $('#page-nav-wrapper').offset().top;
         
         if ( (topDistance) > scrollTop ) {
            $('#page-nav-wrapper').removeClass('fixed');
            $('body').removeClass('sticky-page-nav');
         }
         else {
            $('#page-nav-wrapper').addClass('fixed');
            $('body').addClass('sticky-page-nav');
         }

    });
    
    /* ======= Chart ========= */
    
    $('.chart').easyPieChart({		
		barColor:'#00BCD4',//Pie chart colour
		trackColor: '#e8e8e8',
		scaleColor: false,
		lineWidth : 5,
		animate: 2000,
		onStep: function(from, to, percent) {
			$(this.el).find('span').text(Math.round(percent));
		}
	});  
	

    
    /* ======= Isotope plugin ======= */
    /* Ref: http://isotope.metafizzy.co/ */
    // init Isotope    
    var $container = $('.isotope');
    
    $container.imagesLoaded(function () {
        $('.isotope').isotope({
            itemSelector: '.item'
        });
    });
    
    // filter items on click
    $('#filters').on( 'click', '.type', function() {
      var filterValue = $(this).attr('data-filter');
      $container.isotope({ filter: filterValue });
    });
    
    // change is-checked class on buttons
    $('.filters').each( function( i, typeGroup ) {
        var $typeGroup = $( typeGroup );
        $typeGroup.on( 'click', '.type', function() {
          $typeGroup.find('.active').removeClass('active');
          $( this ).addClass('active');
        });
    });
    

});

$("#submit").on('click',function(event) {
    //alert('click success');
    event.preventDefault();
    $(this).attr('disabled','disabled');

        var url = 'http://localhost:8088/contactUs/4d39e111-aec8-467b-944c-5d5c1de75c9d';
        var data = {
            mobile: $('#mobile').val(),
            name: $('#name').val(),
            email: $('#email').val(),
            address: ($("#address").val())? $("#address").val() : null,
            message: ($("#message").val())? $("#message").val() : null
           
        };
       
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            crossDomain: true,
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function(data){
                alert('success');
                },
            error: function(data) {
                alert('failed');
                }
        });

});


$("#hire").on('click',function(event) {
    //alert('click success');
    event.preventDefault();
    $(this).attr('disabled','disabled');

        var url = 'http://localhost:8088/contactUs/4d39e111-aec8-467b-944c-5d5c1de75c9d';
        var data = {
            mobile: $('#hmobile').val(),
            name: $('#hname').val(),
            email: $('#hemail').val(),
            message: ($("#hmessage").val())? $("#hmessage").val() : null,
          
        };
       
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            crossDomain: true,
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function(data){
                alert('success');
                },
            error: function(data) {
                alert('failed');
                }
        });

});


