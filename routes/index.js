var env = require('../env');    
var config = require('../config/' + env.name);
var Client = require('node-rest-client').Client;
var client = new Client();

module.exports.index = index;
module.exports.blogDetail = blogDetail;

function index(req,res) {
	var meCallback = function(error, data){
		if(data && data.error == undefined){
			res.render('index',{
				layout:'layout',
				title:'portfolio',
				portfolio: data.portfolio,
				project:data.projects,
				blog:data.blog
			});
		}else{
			res.render('500',{
				layout:'layout',
				title:'Server Error'
			});
		}
	}
	var args = {
	  headers: { "Content-Type": "application/json", "Accepts":"application/json"}
	};
	var url =  config.api.url + "/api/portfolio?apikey="+ config.api.key;
	var req = client.get(url, args, function (data, response) {
		if (data && data.error == undefined){
			cCallback(null, data);
		}else{
			meCallback(data.error, null);
		}
	});

	var cCallback = function(error, data){
        var cUrl =  config.api.url+ "/api/projects?apikey=" + config.api.key+"&psize=2000"+"&pno=1";
        var cData =data;
        var req = client.get(cUrl, args, function (data, response) { 
            if (data && data.error == undefined){
			cData.projects = data.data;
                blogCallback(null, cData);
            }else{
                meCallback(data.error, null);
            }
        });
	};
	var blogCallback = function(error, data){
		var blogUrl =  config.api.url+ "/api/blogs"+"?apikey=" + config.api.key+"&psize=3"+"&pno=1";
		var aData =data;
		var req = client.get(blogUrl, args, function (data, response) { 
			
			if (data && data.error == undefined){
				// console.log('---------'+JSON.stringify(data));
			aData.blog = data.data;
			meCallback(null, aData);
			}else{
			blogCallback(data.error, null);
			}
		});
	};
};

function blogDetail(req,res){
   var uName = req.params.uname;
   var meCallback = function(error, data){
	  // console.log(JSON.stringify(data));
	   if(data && data.error == undefined){

		   res.render('blog-detail',{
				layout:'layout',
			   title:'blog',
			   portfolio:data.portfolio,
			   blog: data.data[0],
		   });
	   }else{
		   res.render('500',{
			   layout:'layout',
			   title:'Server Error'
		   });
	   }
    }
   var args = {
	 headers: { "Content-Type": "application/json", "Accepts":"application/json"}
   };
   var url =  config.api.url + "/api/blogs/"+uName+"?apikey="+ config.api.key;
  
   var req = client.get(url, args, function (data, response) {
	   if (data && data.error == undefined){
		   cCallback(null, data);
	   }else{
		   meCallback(data.error, null);
	   }
   });

   var cCallback = function(error, data){
		var cUrl =  config.api.url+ "/api/portfolio?apikey=" + config.api.key+"&psize=2000"+"&pno=1";
			var cData =data;
			var req = client.get(cUrl, args, function (data, response) { 
				if (data && data.error == undefined){
					console.log(JSON.stringify(data));
				cData.portfolio = data.portfolio;
					meCallback(null, cData);
				}else{
					meCallback(data.error, null);
				}
			});
		};
    };
